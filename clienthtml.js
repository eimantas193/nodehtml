var ip = '192.168.8.107';
var net = require('net');
var client = net.connect({port: 3000, host: ip}, function(){
    console.log('connected to server port 3000');

    client.write('GET / HTTP/1.1\nHost: 127.0.0.1\n\n');
    client.pipe(client);

    client.on('data', function(data) {
        console.log(data.toString());
        client.end();
     });
    //client.end();
 
 client.on('end', function() { 
    console.log('disconnected from server port 3000');
 });
});